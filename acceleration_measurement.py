import time
import h5py
import adafruit_adxl34x
import numpy as np
import board
import json
import os

from functions.m_operate import prepare_metadata
from functions.m_operate import log_JSON
from functions.m_operate import set_sensor_setting

"""Parameter definition"""
# -------------------------------------------------------------------------------------------#1-start
# TODO: Adjust the parameters to your needs
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
path_setup_json = "datasheets/setup_mixer.json"  # adjust this to the setup json path
measure_duration_in_s = 20

# ---------------------------------------------------------------------------------------------#1-end

"""Prepare Metadata and create H5-File"""
(
    setup_json_dict,
    sensor_settings_dict,
    path_h5_file,
    path_measurement_folder,
) = prepare_metadata(path_setup_json, path_folder_metadata="datasheets")

print("Setup dictionary:")
print(json.dumps(setup_json_dict, indent=2, default=str))
print()
print("Sensor settings dictionary")
print(json.dumps(sensor_settings_dict, indent=2, default=str))
print()
print(f"Path to the measurement data h5 file created: {path_h5_file}")
print(f"Path to the folder in which the measurement is saved: {path_measurement_folder}")


"""Establishing a connection to the acceleration sensor"""
i2c = board.I2C()  # use default SCL and SDA channels of the pi
try:
    accelerometer = adafruit_adxl34x.ADXL345(i2c)
except Exception as error:
    print(
        "Unfortunately, the ADXL345 accelerometer could not be initialized.\n \
           Make sure your sensor is wired correctly by entering the following\n \
           to your pi's terminal: 'i2cdetect -y 1' "
    )
    print(error)


# -------------------------------------------------------------------------------------------#2-start
# TODO: Initialize the data structure
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#create dictionary
uuid_dictionary_sensor = {"accelerometer": "1ee847be-fddd-6ee4-892a-68c4555b0981"}

#list
acceleration_uuids = {uuid: {"acceleration_x": [], "acceleration_y": [], "acceleration_z": [], "timestamp": []} for uuid in uuid_dictionary_sensor.values()}


# ---------------------------------------------------------------------------------------------#2-end


# -------------------------------------------------------------------------------------------#3-start
# TODO: Measure the probe
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
i2c = board.I2C()

accelerometer = adafruit_adxl34x.ADXL345(i2c)

start_time = time.time()

timestamp = round(time.time() - start_time)

while (time.time() - start_time) <= measure_duration_in_s:
    
    acceleration_uuids["1ee847be-fddd-6ee4-892a-68c4555b0981"]["acceleration_x"].append(accelerometer.acceleration[0])  #acceleration_x
    acceleration_uuids["1ee847be-fddd-6ee4-892a-68c4555b0981"]["acceleration_y"].append(accelerometer.acceleration[1])  #acceleration_y
    acceleration_uuids["1ee847be-fddd-6ee4-892a-68c4555b0981"]["acceleration_z"].append(accelerometer.acceleration[2])  #acceleration_z
    acceleration_uuids["1ee847be-fddd-6ee4-892a-68c4555b0981"]["timestamp"].append(timestamp)
    
    time.sleep(0.001)

# ---------------------------------------------------------------------------------------------#3-end

# -------------------------------------------------------------------------------------------#4-start
# TODO: Write results in hdf5
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Pfad zur HDF5-Datei
hdf5_file_path = path_h5_file
# HDF5_Datei öffnen und Messungsergebnisse in die jeweiligen Ordner speichern
with h5py.File(hdf5_file_path, "a") as hdf5_file:
    for uuid, data in acceleration_uuids.items():
        group = hdf5_file.create_group(uuid)
        group.create_dataset("acceleration_x", data=data["acceleration_x"])
        group.create_dataset("acceleration_y", data=data["acceleration_y"])
        group.create_dataset("acceleration_z", data=data["acceleration_z"])
        group.create_dataset("timestamp", data=data["timestamp"])

        group["acceleration_x"].attrs["units"] = "m/s²"
        group["acceleration_y"].attrs["units"] = "m/s²"
        group["acceleration_z"].attrs["units"] = "m/s²"
        group["timestamp"].attrs["units"] = "miliseconds"



# ---------------------------------------------------------------------------------------------#4-end

"""Log JSON metadata"""
log_JSON(setup_json_dict, path_setup_json, path_measurement_folder)
print("Measurement data was saved in {}/".format(path_measurement_folder))
